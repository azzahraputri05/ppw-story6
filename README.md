# Story Perancangan & Pemrograman Web Fasilkom UI 2018

##	Nama
Azzahra Putri Ramadhanty
PPW D
1706025125

## Herokuapp Link
https://story6-ppw.herokuapp.com

## Website Status
[![pipeline status](https://gitlab.com/azzahraputri05/ppw-story6/badges/master/pipeline.svg)](https://gitlab.com/azzahraputri05/ppw-story6/commits/master)

[![coverage report](https://gitlab.com/azzahraputri05/ppw-story6/badges/master/coverage.svg)](https://gitlab.com/azzahraputri05/ppw-story6/commits/master)
