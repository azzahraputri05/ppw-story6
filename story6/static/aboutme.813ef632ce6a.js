$(document).ready(function($) {
    $('.accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');

      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');

    });
});


$(document).ready(function(){
  $(function() {
    $("#accordion").accordion();
  });
  $("#switch").click(function(){
    if ($(this).is(":checked")) {
      $("body").css("background","black");
      $("h1").css("color", "#E6E6FA");
      $("label").css("color", "white");
    } else{
      $("body").css("background","linear-gradient(to top, #ff33cc 0%, #6666ff 100%)");
      $("h1").css("color", "black");
    }
  })
});

// jQuery(document).ready(function($) {  

// // site preloader -- also uncomment the div in the header and the css style for #preloader
// $(window).load(function(){
//   $('#preloader').fadeOut('slow',function(){$(this).remove();});
// });

// });
$(window).load(function() {
  $(".loader").delay(2000).fadeOut("slow");
  $("#overlayer").delay(2000).fadeOut("slow");
})