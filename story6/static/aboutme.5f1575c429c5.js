$(document).ready(function(){
  $(function() {
    $("#accordion").accordion();
  });
  $("#switch").click(function(){
    if ($(this).is(":checked")) {
      $("body").css("background","black");
      $("h1").css("color", "#E6E6FA");
      $("label").css("color", "white");
    } else{
      $("body").css("background","linear-gradient(to top, #ff33cc 0%, #6666ff 100%)");
      $("h1").css("color", "black");
    }
  })
});