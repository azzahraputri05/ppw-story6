var counter = 0;
function changeStar(id){
  var star = $('#'+id).html();
  if(star.includes("gray")) {
      counter++;
      $('#'+id).html("<i class='fa fa-star' style='color : #DA70D6'></i>");
      $("#fav").html("<i class='fa fa-star'style='color : #DA70D6'></i> " + counter + " ");
  }
  else{
      counter--;
      $('#'+id).html("<i class='fa fa-star' style='color : gray'></i>");
      $("#fav").html("<i class='fa fa-star'style='color : #DA70D6'></i> " + counter + " ");
  }   
}

$(document).ready(function(){
  cari("quilting");
});

$("#search").click(function(){
  var textbox = $("#searchbar").val();
  cari(textbox);
});

function cari(txt) {
  $.ajax({
    url: "/jsonfile?cari=" + txt,
    success: function(result){
      result = result.items;
      var header = "<thead><tr><th>Title</th> <th>Author</th> <th>Cover</th> <th>Description</th> <th></th> </tr></thead>";
      $("#result thead").remove();
      $("#result").append(header);
      $("#result tbody").remove();
      $("#result").append("<tbody>");

      for(i=0; i<result.length; i++){
        var content = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" 
        + result[i].volumeInfo.authors + "</td><td>" + "<img src='"
        + result[i].volumeInfo.imageLinks.thumbnail+ "'>"+"</td><td>" 
        + result[i].volumeInfo.description +"</td><td>" 
        + "<button class='button' style = 'background-color: Transparent; border: none' id='"
        + result[i].id + "' onclick = 'changeStar(" +"\""+result[i].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";
        $("#result").append(content);
      }
      $("#result").append("</tbody>");
    }
  })

}


    //        $(".fav").each(function() {
    //             $(this).click(function() {
    //                 if($(this).hasClass("far")) {
    //                     $(this).removeClass("far");
    //                     $(this).removeClass("fa-heart");
    //                     $(this).addClass("fas");
    //                     $(this).addClass("fa-heart");
    //                 }else {
    //                     $(this).removeClass("fas");
    //                     $(this).removeClass("fa-heart");
    //                     $(this).addClass("far");
    //                     $(this).addClass("fa-heart");
    //                 }
    //             } );
    //             });
    //     },
    //     type: 'GET'


    // });