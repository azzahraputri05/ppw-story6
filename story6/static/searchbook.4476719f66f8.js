var counter = 0;
        function changeStar(id){
            var star = $('#'+id).html();
            if(star.includes("gray")) {
                counter++;
                $('#'+id).html("<i class='fa fa-star' style = 'color : red'></i>");
                $("#fav").html("<i class='fa fa-star'style = 'color : red'></i> " +counter + " ");
            }
            else{
                counter--;
                $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
                $("#fav").html("<i class='fa fa-star' style = 'color : red'></i> " +counter + " ");
            }   
        }

$(document).ready(function(){
	cari("quilting");
});

$("#search").click(function(){
	var textbox = $("#searchbar").val();
	cari(textbox);
});

function cari(txt) {
	$.ajax({
		url: "/jsonfile?cari=" + txt,
		success: function(result){
			result = result.items;
			var header = "<thead><tr><th>Title</th> <th>Author</th> <th>Cover</th> <th>Description</th> <th>Publisher</th> <th>Published Date</th> <th></th> </tr><head>";
			$("#table thead").remove();
			$("#table").append(header);
			$("#table tbody").remove();
			$("#table").append("<tbody>");

			for(i=0; i<result.length; i++){
				var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>"+"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>"+"</td><td>" + result[i].volumeInfo.description +"</td><td>" + result[i].volumeInfo.publisher +"</td><td>"+ result[i].volumeInfo.publishedDate +"</td><td>" +"<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'changeStar(" +"\""+result[i].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";
				$("#table").append(tmp);
			}
			$("#table").append("</tbody>");
		}
	})

}
