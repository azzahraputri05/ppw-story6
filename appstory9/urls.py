from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'appstory9'

urlpatterns = [
	path('searchbook', views.searchbook, name="search"),
	path('jsonfile', views.jsonfile, name="jsonfile"),
	path('increasefav', views.increasefav, name='increasefav'),
	path('decreasefav', views.decreasefav, name='decreasefav'),
	path('logout', views.logout_view , name='logout'),
]