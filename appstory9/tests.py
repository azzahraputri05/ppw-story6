from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import searchbook, jsonfile, increasefav, decreasefav

# Create your tests here.


class Lab9UnitTest(TestCase):
	def test_lab_9_list_book_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_json_file_is_exist(self):
		response = Client().get('/jsonfile?cari=')
		self.assertEqual(response.status_code, 200)

	def test_lab_11_using_books_template(self):
		response = Client().get('/searchbook')
		self.assertTemplateUsed(response, 'searchbook.html')

	def test_lab_11_url_is_exist(self):
		response = Client().get('/searchbook')
		self.assertEqual(response.status_code, 200)

	def test_lab11_using_func(self):
		found = resolve('/searchbook')
		self.assertEqual(found.func, searchbook)


