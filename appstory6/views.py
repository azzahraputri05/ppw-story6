from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import SubscribeForm
from .models import Subscribe

# Create your views here.

# def addform(request):
#     form = Message_Form(request.POST or None)
#     response = {}
#     if(request.method == "POST"):
#         if (form.is_valid()):
#             status = request.POST.get('status')
#             Entry.objects.create(status=status)
#             print(status)
#             return HttpResponseRedirect('/')

#         else:
#             return render(request, "form.html", response)
#     else:
#     	status = Entry.objects.all()
#     	response['form'] = form
#     	response['status'] = status
#     	return render(request, 'form.html', response)
    
# def profile(request):
#     response = {}
#     return render(request, 'Profile.html', response)

def aboutme(request):
    response = {}
    return render(request, 'aboutme.html', response)

def subscribe(request):
    response = {}
    response['form'] = SubscribeForm()
    return render(request, 'subscribe.html', response)

def subscribers(request):
    return render(request, "sublist.html") 

@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscribe.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)

def success(request):
    form = SubscribeForm(request.POST or None)
    if (form.is_valid()):
        data = form.cleaned_data
        newsubscriber = Subscribe(name=data['name'], password=data['password'], email=data['email'])
        newsubscriber.save()
        obdata = {'name': data['name']}
    return JsonResponse(obdata)

def getallsubs(request):
    data_json = serializers.serialize('json', Subscribe.objects.all())
    return HttpResponse(data_json, content_type='aplication/json')

@csrf_exempt
def delsubs(request):
    if 'email' in request.POST:
        email = request.POST['email']
        data = Subscribe.objects.filter(email=email)
        data.delete()
        return HttpResponseRedirect("/sublist")
    else:
        return HttpResponseRedirect("/sublist")


