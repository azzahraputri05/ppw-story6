from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'appstory6'

urlpatterns = [
	# path('', views.addform, name="form"),
	# path('profile', views.profile, name="profile"),
	path('', views.aboutme, name='aboutme'),
	path('subscribe', views.subscribe, name='subscribe'),
	path('success', views.success, name='success'),
	path('validate', views.validate),
	path('subscribers', views.subscribers, name="subscribers"),
	path('sublist', views.getallsubs, name='sublist'),
	path('delsubs', views.delsubs)
] 