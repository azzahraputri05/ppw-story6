from django.test import TestCase
from django.test import Client
from django.urls import resolve
# from .views import addform, profile, aboutme
# from .models import Entry
# from .forms import Message_Form
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import time
from .views import aboutme, subscribe, validate, success
from .forms import SubscribeForm
from .models import Subscribe
from django.utils.encoding import force_text


# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_lab6_profile_has_name(self):
		response = Client().get('/')
		string = response.content.decode('utf8')
		self.assertIn('Nama: Azzahra Putri Ramadhanty', string)

	def test_lab6_profile_has_dateofbirth(self):
		response = Client().get('/')
		string = response.content.decode('utf8')
		self.assertIn('Date of Birth: Jakarta, January 5 1999', string)

	def test_lab6_profile_has_Hobby(self):
		response = Client().get('/')
		string = response.content.decode('utf8')
		self.assertIn('Hobby: Travelling, watching Youtube, etc.', string)

	def test_lab10_url_is_exist(self):
		response = Client().get('/subscribe')
		self.assertEqual(response.status_code, 200)

	def test_lab10_using_subscribe_func(self):
		found = resolve('/subscribe')
		self.assertEqual(found.func, subscribe)


	def test_double_subscriber(self):
		nama = 'abcde'
		password = 'opqrstu'
		email = 'vwxyz@gmail.com'
		go = Client().post('/validate', {'email': email})
		self.assertJSONEqual(
		force_text(go.content),
			{'not_valid': False} #responds false
		)

		# button not disabled, send data to views
		Client().post('/success', {'name': nama, 'password': password, 'email': email})
		subsum = Subscribe.objects.all().count()
		self.assertEqual(subsum, 1)

		# email already taken
		go = Client().post('/validate', {'email': email})
		self.assertJSONEqual(
		str(go.content, encoding='utf8'),
			{'not_valid': True} 
		)

		# not making any new data
		subsum = Subscribe.objects.all().count()
		self.assertEqual(subsum, 1)


# class Lab7FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25) 
#         super(Lab7FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab7FunctionalTest, self).tearDown()

#     def test_input_todo(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/')
#         # find the form element
#         form = selenium.find_element_by_name('status')
#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         form.send_keys('Coba Coba')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         time.sleep(5)
#         self.assertIn('Coba Coba', selenium.page_source)

#     def test_layout_profile(self):
#     	selenium = self.selenium
#     	selenium.get('http://story6-ppw.herokuapp.com/profile')
#     	header = selenium.find_element_by_tag_name('h1').text
#     	self.assertIn('MY PROFILE', header)

#     	bio = selenium.find_element_by_tag_name('h3').text
#     	self.assertIn('Nama: Azzahra Putri Ramadhanty\nDate of Birth: Jakarta, January 5 1999\nHobby: Travelling, watching Youtube, etc.', bio)
#     	time.sleep(1)

#     def test_css_is_correct(self):
#     	selenium = self.selenium
#     	selenium.get('http://story6-ppw.herokuapp.com')
#     	col_css = selenium.find_element_by_css_selector('div.col-sm-12')
#     	button_css = selenium.find_element_by_css_selector('button.registerbtn')
#     	time.sleep(1)


